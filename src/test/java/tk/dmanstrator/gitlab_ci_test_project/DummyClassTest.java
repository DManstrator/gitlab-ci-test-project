package tk.dmanstrator.gitlab_ci_test_project;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class DummyClassTest {

    @Test
    void testGetTestValue()  {
        final DummyClass dummyClass = new DummyClass();
        Assertions.assertEquals(dummyClass.getTestValue(), 4711);
    }

}